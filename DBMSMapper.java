package project;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class DBMSMapper extends Mapper<LongWritable, Text, Text, CountTempSum> {

    public static Integer getInt(String str) {
        StringBuilder strb = new StringBuilder();
        char[] array = str.toCharArray();
        for (char i : array) {
            if (i >= '0' && i <= '9') {
                strb.append(i);
            }
        }
        if (strb.length() != 0) {
            return Integer.parseInt(strb.toString());
        } else
            return null;
    }

    public Map<Integer, String> getStationState(BufferedReader bfr) throws IOException {
        Map<Integer, String> stationState = new HashMap<Integer, String>();

        // Ignore First Line
        bfr.readLine();
        for (String line = bfr.readLine(); null != line; line = bfr.readLine()) {
            StringTokenizer strk = new StringTokenizer(line, ",");
            Integer USAF = getInt(strk.nextToken());
            if (null == USAF)
                continue;

            // ignore WBAN
            strk.nextToken();
            // ignore STATION NAME
            strk.nextToken();
            // ignore CTRY - Maybe has to take care that it is only US
            String country = strk.nextToken().replaceAll("\"", "").trim();
            if (!"US".equals(country))
                continue;

            // get State
            String state = strk.nextToken();
            if (null == state && state.length() == 0)
                continue;
            state = state.replaceAll("\"", "").trim();
            if (state.length() == 0)
                continue;

            stationState.put(USAF, state);
        }

        System.out.println(stationState.size());
        return stationState;
    }
    
    private Map<Integer, String> stationState = null;

    @Override
    public void setup(Context context) throws IOException {
        Configuration conf = context.getConfiguration();
        String weatherStationLocFilename = conf.get(DBMSProject.STATION_STATE_PROP);
        Path path = new Path(weatherStationLocFilename);
        try {
            FileSystem fs = path.getFileSystem(conf);
            BufferedReader bfr=new BufferedReader(new InputStreamReader(fs.open(path)));
            stationState = getStationState(bfr);
            bfr.close();    
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw e;
        }
        
    }
    
    @Override
    public void map(LongWritable lineID, Text line, Context context) throws IOException, InterruptedException {
        StringTokenizer strk = new StringTokenizer(line.toString());
        
        Integer station = 0;
        Double temperature = 0d;
        
        //Station Id
        String stnstr = null;
        try{
            stnstr = strk.nextToken(); 
            station = Integer.parseInt(stnstr);
        }catch(NumberFormatException e){
            System.err.println("STN Not an integer : "+ stnstr);
            return;
        }
        
        //Ignore WBAN
        strk.nextToken();
        
        //Retrieve Month   
        String month = strk.nextToken().trim().substring(4,6);
     
        //Get Temperature
        String tempstr = null;
        try{
            tempstr = strk.nextToken(); 
            temperature = Double.parseDouble(tempstr);
        }catch(NumberFormatException e){
            System.err.println("Temp Not a double : "+ tempstr);
            return;
        }
        
        if (stationState.containsKey(station)) {
            String state = stationState.get(station);
            Text key = new Text(state + " " + month);
            CountTempSum value = new CountTempSum(1, temperature); // value
            context.write(key, value);
        }
    }

    @Override
    public void cleanup(Context context) {
    }
}
