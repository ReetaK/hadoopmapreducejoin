package project;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class DBMSProject extends Configured implements Tool {

    public final static String STATION_STATE_PROP = "path.stationState";
    public final static String OUTPUT_FILE_PROP = "path.outputFile";

    @Override
    public int run(String[] args) throws Exception {
        Configuration conf = getConf();
        Job job = Job.getInstance(conf, "Check-MapReduce");
        if (job == null)
            return -1;

        if (null == conf.get(DBMSProject.STATION_STATE_PROP)) {
            System.err.println(
                    "Please Specify property -D " + STATION_STATE_PROP + " <hdfs://Location of WeatherStation.csv>");
            return 1;
        }

        if (null == conf.get(OUTPUT_FILE_PROP)) {
            System.err.println("Please Specify property -D " + OUTPUT_FILE_PROP
                    + "<hdfs://Location of final output file> | <file://Location of final output file>");
            return 1;
        }

        job.setJarByClass(DBMSProject.class);

        job.setInputFormatClass(TextInputFormat.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(CountTempSum.class);
        job.setMapperClass(DBMSMapper.class);

        job.setCombinerClass(DBMSCombiner.class);

        job.setReducerClass(DBMSReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(DoubleWritable.class);

        job.setOutputFormatClass(TextOutputFormat.class);

        job.submit();
        boolean success = job.waitForCompletion(true);

        if (!success)
            return 1;

        List<Path> outputFiles = getOutputFilesPath(job);
        
        TreeMap<Double, List<TempDataComb>> sortTempState = getStateRank(outputFiles, job.getConfiguration());
        // Write SortedMap in OutputFile

        writeSortStateTemp(conf.get(OUTPUT_FILE_PROP), job.getConfiguration(), sortTempState);

        return 0;
    }

    private void writeSortStateTemp(String outputPath, Configuration conf, TreeMap<Double, List<TempDataComb>> sortTempState)
            throws IOException {
        Path path = new Path(outputPath);
        try {
            FileSystem fs = path.getFileSystem(conf);
            BufferedWriter bfw = new BufferedWriter(new OutputStreamWriter(fs.create(path)));
            for (Double temperature : sortTempState.keySet()) {
                for (TempDataComb data : sortTempState.get(temperature)) {
                    String writeString = data.toString();
                    bfw.write(writeString);
                    bfw.newLine();
                }
            }
            bfw.flush();
            bfw.close();
        } catch (IOException e) {
            System.err.println("Failed to write in " + outputPath);
            throw e;
        }
    }

    private static class TempDataComb {
        private static String[] monthName =
        {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

        private String state;
        private double maxTemp;
        private double minTemp;
        private int maxMonth;
        private int minMonth;
        
        private String getMonth(int num) {
            if (num < 1 || num > 12) 
                return null;
            else
                return monthName[num - 1];
        }

        TempDataComb(String state, double maxTemp, double minTemp, int maxMonth, int minMonth) {
            this.state = state;
            this.maxTemp = maxTemp;
            this.minTemp = minTemp;
            this.maxMonth = maxMonth;
            this.minMonth = minMonth;
        }
        
        @Override
        public String toString() {
            StringBuilder strb = new StringBuilder();
            strb.append("\"");
            strb.append(state);
            strb.append("\",");
            
            strb.append("\"");
            strb.append(maxTemp);
            strb.append(",");
            strb.append(getMonth(maxMonth));
            strb.append("\",");
            
            strb.append("\"");
            strb.append(minTemp);
            strb.append(",");
            strb.append(getMonth(minMonth));
            strb.append("\",");
            
            double tempDiff = maxTemp - minTemp;
            strb.append("\"");
            strb.append(tempDiff);
            strb.append("\"");
            
            return strb.toString();
        }

    }

    private TreeMap<Double, List<TempDataComb>> getStateRank(List<Path> pathList, Configuration conf) throws IOException {
        Map<String, Double> stateMaxTemp = new HashMap<String, Double>();
        Map<String, Double> stateMinTemp = new HashMap<String, Double>();
        Map<String, Integer> stateMaxTempMonth = new HashMap<String, Integer>();
        Map<String, Integer> stateMinTempMonth = new HashMap<String, Integer>();

        for (Path path : pathList) {
            FileSystem fs = path.getFileSystem(conf);
            BufferedReader bfr = new BufferedReader(new InputStreamReader(fs.open(path)));
            
            for (String line = bfr.readLine(); line != null; line = bfr.readLine()) {
                StringTokenizer strk = new StringTokenizer(line);
                String state = strk.nextToken().trim();
                Integer month = Integer.parseInt(strk.nextToken().trim());
            
                Double temp = Double.parseDouble(strk.nextToken().trim());

                // Compare it with max temperature of the state
                if (stateMaxTemp.containsKey(state)) {
                    if (temp > stateMaxTemp.get(state)) {
                        stateMaxTemp.put(state, temp);
                        stateMaxTempMonth.put(state, month);
                    }
                } else {
                    stateMaxTemp.put(state, temp);
                    stateMaxTempMonth.put(state, month);
                }
                
                // Compare it with min temperature of the state
                if (stateMinTemp.containsKey(state)) {
                    if (temp < stateMinTemp.get(state)) {
                        stateMinTemp.put(state, temp);
                        stateMinTempMonth.put(state, month);
                    }
                } else {
                    stateMinTemp.put(state, temp);
                    stateMinTempMonth.put(state, month);
                }
            }
            bfr.close();
        }

        TreeMap<Double, List<TempDataComb>> sortedStateList = new TreeMap<Double, List<TempDataComb>>();
        
        for (String state : stateMaxTemp.keySet()) {
            Double maxTemp = stateMaxTemp.get(state);
            Double minTemp = stateMinTemp.get(state);
            Double tempDiff = maxTemp - minTemp;
            Integer maxMonth = stateMaxTempMonth.get(state);
            Integer minMonth = stateMinTempMonth.get(state);
            
            TempDataComb newData = new TempDataComb(state, maxTemp, minTemp, maxMonth, minMonth);
            
            if (sortedStateList.containsKey(tempDiff)) {
                List<TempDataComb> array = sortedStateList.get(tempDiff);
                array.add(newData);
                sortedStateList.put(tempDiff, array);
            } else {
                ArrayList<TempDataComb> array = new ArrayList<TempDataComb>();
                array.add(newData);
                sortedStateList.put(tempDiff, array);
            }
        }

        return sortedStateList;
    }

    private List<Path> getOutputFilesPath(Job job) throws IOException {
        // Get the outputDirectory
        Path outputPath = FileOutputFormat.getOutputPath(job);
        // Get the list of Files in outputDirectory
        FileSystem fs = outputPath.getFileSystem(job.getConfiguration());
        FileStatus[] status = fs.listStatus(outputPath);

        List<Path> pathList = new ArrayList<Path>(status.length);
        for (FileStatus oneStatus : status) {
            if (oneStatus.isFile() && oneStatus.getLen() != 0) {
                pathList.add(oneStatus.getPath());
            }
        }
        return pathList;
    }

    public static void main(String[] args) throws Exception {
        int exitCode = ToolRunner.run(new DBMSProject(), args);
        System.exit(exitCode);
    }

}
