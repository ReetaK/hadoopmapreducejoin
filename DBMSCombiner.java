package project;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class DBMSCombiner extends Reducer<Text, CountTempSum,Text, CountTempSum> {
    
    @Override
    public void reduce(final Text key, Iterable<CountTempSum> values, Context context) throws IOException, InterruptedException {
        long count = 0L;
        double tempSum = 0D;
        
        for (CountTempSum value : values) {
            count += value.getCount();
            tempSum += value.getTempSum();
        }
        
        CountTempSum value = new CountTempSum(count, tempSum);
        context.write(key, value);
    }

}
