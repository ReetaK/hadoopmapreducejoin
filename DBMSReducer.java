package project;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class DBMSReducer extends Reducer<Text, CountTempSum, Text, DoubleWritable> {
    
    @Override
    public void setup(Context context) {
    }
    
    @Override
    public void reduce(Text stateMonth, Iterable<CountTempSum> values, Context context) throws IOException, InterruptedException {
        //Find the average of the temperature
        
        Double sumTemp = 0D;
        Long count = 0L;
        
        for (CountTempSum value : values) {
            count += value.getCount();
            sumTemp += value.getTempSum();
        }
        
        Double average = sumTemp / count;
        context.write(stateMonth, new DoubleWritable(average));
    }
    
    @Override
    public void cleanup(Context context) {
    }
}
