package project;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;

//This is a count, temperature summation which is also writable
public class CountTempSum implements Writable {
    //Sum of Temperatures
    private double sumTemp;
    //Number of Temperatures
    private long count;
    
    CountTempSum() {
        sumTemp = 0D;
        count = 0;
    }
    
    CountTempSum(long count ,double sumTemp) {
        this.sumTemp = sumTemp;
        this.count = count; 
    }
    
    public long getCount() {
        return count;
    }
    
    public double getTempSum() {
        return sumTemp;
    }
    
    @Override
    public void readFields(DataInput in) throws IOException {
        count = in.readLong();
        sumTemp = in.readDouble();   
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeLong(count);
        out.writeDouble(sumTemp);       
    }
    
}
